package com.tikalk.reactorworkshop;

import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

public class Ex1_CreatorsTests {

  @Test
  public void test_expectHello() {
    StepVerifier.create(Ex1_Creators.emitHello())
        .expectNext(Ex1_Creators.HELLO)
        .verifyComplete();
  }

  @Test
  public void test_expectNothing() {
    StepVerifier.create(Ex1_Creators.emitNothing())
        .expectNextCount(0)
        .verifyComplete();
  }

  @Test
  public void test_expectError() {
    StepVerifier.create(Ex1_Creators.emitError())
        .expectError(RuntimeException.class)
        .verify();
  }

  @Test
  public void test_getFromFuture() {
    StepVerifier.create(Ex1_Creators.getFromFuture())
        .expectNext(Ex1_Creators.HELLO)
        .verifyComplete();
  }
}
