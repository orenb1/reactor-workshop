package com.tikalk.reactorworkshop;

import com.tikalk.reactorworkshop.util.Helpers;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class Ex4_BlockingOpsTests {

  @Test
  public void test_getNumberFromSlowSource() {
    StepVerifier.create(Ex4_BlockingOps.getNumberFromSlowSource())
        .expectNext(33)
        .verifyComplete();
  }

  @Test
  public void test_publishNumbersToSlowSink() {
    Flux<Integer> flux = Ex4_BlockingOps.publishNumbersToSlowSink().doOnNext(i -> Helpers.mainThreadChecker());
    StepVerifier.create(flux)
        .expectNextCount(100)
        .verifyComplete();
  }
}
