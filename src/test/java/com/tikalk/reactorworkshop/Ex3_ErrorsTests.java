package com.tikalk.reactorworkshop;

import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Ex3_ErrorsTests {

  @Test
  void test_whenErrorStop() {
    StepVerifier.create(Ex3_Errors.ifErrorThenStop())
        .expectNext(1, 2, 3, 4)
        .verifyComplete();
  }

  @Test
  void test_ifErrorThenReturnMinus1() {
    List<Integer> ints = IntStream.range(0, 100)
        .filter(i -> i>0 && i < 6)
        .mapToObj(i -> i % 5 == 0 ? -1 : i)
        .collect(Collectors.toList());

    StepVerifier.create(Ex3_Errors.ifErrorThenReturnMinus1())
        .expectNextSequence(ints)
        .verifyComplete();
  }

    /*@Test
    void test_ifErrorThenReturnMinus1AndContinue() {
        List<Integer> ints = IntStream.range(0, 100)
                .filter(i -> i>0)
                .mapToObj(i -> i % 5 == 0 ? -1 : i)
                .collect(Collectors.toList());

        StepVerifier.create(Ex3.)
    }*/

  @Test
  void test_ifErrorThenOmitAndContinue() {

    List<Integer> ints = IntStream.range(0, 100)
        .filter(i -> i > 0)
        .filter(i -> i % 5 != 0)
        .mapToObj(i -> i)
        .collect(Collectors.toList());

    StepVerifier.create(Ex3_Errors.ifErrorThenOmitAndContinue())
        .expectNextSequence(ints)
        .verifyComplete();
  }

  @Test
  void test_errorOnCreate() {
    StepVerifier.create(Ex3_Errors.errorOnCreate())
        .expectNext(-1)
        .verifyComplete();
  }

  @Test
  void test_innerErrorHandling() {
    StepVerifier.create(Ex3_Errors.innerErrorHandling().filter(s -> "-1".equals(s)))
        .expectNextCount(1)
        .verifyComplete();
  }
}
