package com.tikalk.reactorworkshop;

import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

public class Ex2_TransformationsTests {

  @Test
  public void test_expectAllNumbersUptoNDividedByM() {
    int randomNum = new SecureRandom().nextInt(1000);
    int randomDivider = new SecureRandom().nextInt(randomNum);

    List<Integer> ints = getNDividedByM(randomNum, randomDivider);

    System.out.println("randomNum: " + randomNum + " randomDivider: " + randomDivider);
    StepVerifier.create(Ex2_Transformations.getAllNumbersUptoN_DividedByM(randomNum, randomDivider))
        .expectNextSequence(ints)
        .verifyComplete();
  }

  private List<Integer> getNDividedByM(int randomNum, int randomDivider) {
    List<Integer> ints = IntStream.range(0, randomNum)
        .filter(i -> i % randomDivider == 0)
        .mapToObj(i -> i)
        .collect(Collectors.toList());
    return ints;
  }

  @Test
  public void test_squareThrowErrorWhenDividedBy5() {
    List<Integer> ints = getNDividedByM(100, 3).stream()
        .filter(i -> i < 15)
        .map(i -> i*i)
        .collect(Collectors.toList());

    StepVerifier.create(Ex2_Transformations.squareThrowErrorWhenDividedBy5Except0())
        .expectNextSequence(ints)
        .expectErrorMatches(th -> th instanceof RuntimeException && "225".equals((th.getMessage())))
        .verify();
  }
}
