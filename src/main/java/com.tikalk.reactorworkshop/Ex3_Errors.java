package com.tikalk.reactorworkshop;

import com.tikalk.reactorworkshop.util.Helpers;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Ex3_Errors {

  public static Flux<Integer> ifErrorThenStop() {
    return Helpers.getIntegerFluxWithErrors();
  }

  /**
   * On error return -1
   */
  public static Flux<Integer> ifErrorThenReturnMinus1() {
    return Helpers.getIntegerFluxWithErrors();
  }

  public static Flux<Integer> ifErrorThenOmitAndContinue() {
    return Helpers.getIntegerFluxWithErrors();
  }

  /**
   * Fix so error will be recovered with value -1
   * @return
   */
  public static Mono<Integer> errorOnCreate() {
    return Mono.just(1/0)
        .onErrorReturn(-1);
  }

  /**
   * Fix code to make onErrorResume work.
   */
  public static Flux<String> innerErrorHandling() {
    return Flux.range(0, 30)
        .flatMap(i ->
            Flux.just(i)
                .doOnNext(n -> {
                  if (n == 10) {
                    throw new IllegalArgumentException("ERROR");
                  }
                })
                .onErrorResume(th -> Flux.just(-1))
        )
        .onErrorContinue((th, o) -> {})
        .map(i -> "" + i);

  }
}
