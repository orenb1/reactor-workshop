package com.tikalk.reactorworkshop;

import com.tikalk.reactorworkshop.util.Helpers;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Ex4_BlockingOps {


  public static Mono<Integer> getNumberFromSlowSource() {
    return Helpers.getNumberBlocking();
  }


  /**
   * Make this flux to be non blocking for all operation downstream (ie - after the method returns)
   */
  public static Flux<Integer> publishNumbersToSlowSink() {
    return Flux.range(0, 100)
        .doOnNext(i -> {
          if (!"main".equals(Thread.currentThread().getName())) {
            throw new IllegalArgumentException("Excepted thread: main");
          }
        });
  }
}
