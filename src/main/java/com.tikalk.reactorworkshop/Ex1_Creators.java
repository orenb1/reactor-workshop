package com.tikalk.reactorworkshop;

import com.sun.net.httpserver.HttpServer;
import com.tikalk.reactorworkshop.util.Helpers;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

// Flux creators
public class Ex1_Creators {

  public static final String HELLO = "Hello";

  public static Mono<String> emitHello() {
    return null;
  }

  public static Flux<Integer> emitNothing() {
    return null;
  }

  /**
   * Throw RuntimeException
   */

  public static Flux<HttpServer> emitError() {
    return null;
  }

  /**
   * Use {@link Helpers#helloFuture()}
   */
  public static Mono<String> getFromFuture() {
    return null;
  }
}
