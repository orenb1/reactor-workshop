package com.tikalk.reactorworkshop;

import reactor.core.publisher.Flux;

public class Ex2_Transformations {

  /**
   * Ie: n=100, m=24 ==> f(n, m) = [0, 24, 48, 72, 96]
   */
  public static Flux<Integer> getAllNumbersUptoN_DividedByM(int n, int m) {
    return null;
  }

  /**
   * Use {@link Ex2_Transformations#getAllNumbersUptoN_DividedByM} (100, 3)
   * For every value, square it. When the value can be divided by 5 without remain, throw RuntimeException with the value.
   */
  public static Flux<Integer> squareThrowErrorWhenDividedBy5Except0() {
    return getAllNumbersUptoN_DividedByM(100, 3);
  }

}
