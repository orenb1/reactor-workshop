package com.tikalk.reactorworkshop.util;

import com.tikalk.reactorworkshop.Ex1_Creators;
import java.util.concurrent.CompletableFuture;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Helpers {

  public static CompletableFuture<String> helloFuture() {
    return CompletableFuture.supplyAsync(() -> {
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return Ex1_Creators.HELLO;
    });
  }

  public static Flux<Integer> rangeOfInts() {
    return Flux.range(0, 100);
  }

  public static Flux<Integer> getIntegerFluxWithErrors() {
    return rangeOfInts()
        .filter(i -> i > 0)
        .doOnNext(i -> {
          if (i % 5 == 0) {
            throw new IllegalArgumentException(i + "");
          }
        });
  }

  public static Mono<Integer> getNumberBlocking() {
    return Mono.defer(() -> {
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
//            e.printStackTrace();
      }

      return Mono.just(33);
    })
        .doOnNext(i -> mainThreadChecker());
  }

  public static void mainThreadChecker() {
    if ("main".equals(Thread.currentThread().getName())) {
      System.err.println("Operation should be non blocking");
      throw new IllegalArgumentException("Operation should be non blocking");
    }
  }

}
