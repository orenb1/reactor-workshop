# Reactor workshop

Workshop for getting to know spring reactor.

## Instructions
There are 4 exercise classes with methods to be fixed.<br>
For each class there's a matching test class named _\<excerise class>Tests_, 
and every test checks a method.

**Mission**: 
Fix exercise classes so all tests will pass.

## Exercise description
### Ex1_Creators
- emitHello - return Mono that emits Ex1_Creators#HELLO 

- emitNothing - return Flux that emits nothing.

- emitError - return Flux that emits error.

- getFromFuture - Using Helpers#helloFuture(), return Mono wrapping the given Future.

### Ex2_Transformations
- getAllNumbersUptoN_DividedByM - Using arguments n & m, return a Flux that emits all numbers
from 0 to n (exclusive), that can be divided by m.
 
- squareThrowErrorWhenDividedBy5Except0 - using Ex2_Transformations#getAllNumbersUptoN_DividedByM,
return a flux that filters 0, emits all values but throws an exception when it sees a value which can be divided by 5. 

### Ex3_Errors
- ifErrorThenStop - Using the helper method, fix the stream to stop whenever an error occurs. 

- ifErrorThenReturnMinus1 -  Using the helper method, fix the stream to return -1 and stop whenever an error occurs.

- ifErrorThenOmitAndContinue -  Using the helper method, fix the stream ignore errors.

- errorOnCreate - Fix the code so the error will be handled by properly.

- innerErrorHandling - Fix code so onErrorResume will e triggered.
- 


### Ex4_BlockingOps 
- getNumberFromSlowSource - Using Helpers.getNumberBlocking(), turn given mono to be non-blocking (Upstream - publisher).
- publishNumbersToSlowSink - Fix code to publish (downstream) to blocking subscriber.  
 


#
##### Good luck